import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PeopleApi from './api/people';

class PeopleList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      people: []
    }

    this.handleDelete = this.handleDelete.bind(this);
  }

  componentDidMount() {
    PeopleApi.getPeople().then((people) => {
      this.setState({people});
    });
  }

  handleDelete(id, index) {
    PeopleApi.delete(id).then((data) => {
      let people = this.state.people;
      people.splice(index, 1);
      this.setState({people});
    });
  }

  render() {
    return (
      <div className="wrapper">
        <div className="top">
          <Link className="btn btn-primary" to="people/add">Agregar persona</Link>
        </div>
        <div>
          <table>
            <thead>
              <tr>
                <th>First name</th>
                <th>Last name</th>
                <th>Age</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              { this.state.people.map((item, key) => {
                return(
                  <tr key={key}>
                    <td>{item.name}</td>
                    <td>{item.lastName}</td>
                    <td>{item.age}</td>
                    <td>
                      <Link to={{
                        pathname: `people/edit/${item.id}`,
                        state: {
                          id: item.id,
                          name: item.name,
                          lastName: item.lastName,
                          age: item.age
                        }
                      }} className="btn btn-edit btn-small">Edit</Link>
                      <Link to='/' className="btn btn-delete btn-small" onClick={(event) => {
                        event.preventDefault();
                        if(confirm("Are you sure?")) {
                          this.handleDelete(item.id, key);
                        }
                      }}>Delete</Link>
                    </td>
                  </tr>
                )
              })}
            </tbody>

          </table>
        </div>
      </div>
    )
  }
}

export default PeopleList;
