import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Styles from '../public/scss/styles.scss';
import PersonForm from './person_form'
import PeopleList from './people_list'

let container = document.getElementById('app');
ReactDOM.render(
  <Router>
    <Switch>
      <Route component={PersonForm} path="/people/edit/:id"/>
      <Route component={PersonForm} path="/people/add"/>
      <Route component={PeopleList} path="/"/>
    </Switch>
  </Router>
  , container);
