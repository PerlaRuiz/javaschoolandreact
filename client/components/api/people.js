import Axios from 'axios';
let base = 'http://localhost:8089/javaschool';

class PeopleApi {

  static getPeople() {
    return Axios.get(`${base}/people`).then((response) => response.data);
  }

  static create(person) {
    return Axios.post(`${base}/people`, person).then((response) => response.data);
  }

  static update(person) {
    return Axios.put(`${base}/people`, person).then((response) => response.data);
  }

  static delete(id) {
    console.log(id);
    return Axios.delete(`${base}/people/${id}`).then((response) => response.data);
  }

}

export default PeopleApi;
