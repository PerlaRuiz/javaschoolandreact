import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PeopleApi from './api/people';

class PersonForm extends Component {

  constructor(props) {
    super(props);
    let id = null;
    let name = '';
    let lastName = '';
    let age = 0;

    if(this.props.location.state) {
      id = this.props.location.state.id;
      name = this.props.location.state.name;
      lastName = this.props.location.state.lastName;
      age = this.props.location.state.age;
    }

    this.state = { id, name, lastName, age }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.state.id) {
      PeopleApi.update(this.state).then((person) => {
        this.props.history.goBack();
      });
    } else {
      PeopleApi.create(this.state).then((person) => {
        this.props.history.goBack();
      });
    }
  }

  handleChange(event) {
    let value = event.target.value;
    let name = event.target.name;
    let lastState = this.state;
    lastState[name] = value;
    this.setState({
      name: lastState.name,
      lastName: lastState.lastName,
      age: lastState.age
    });
  }

  render() {
    let { name, lastName, age } = this.state;
    return(
      <div className="wrapper">
        <h1>Agregar persona</h1>
        <form className="form-people" onSubmit={this.handleSubmit}>

          <div className="field">
            <label>First name:</label>
            <input type="text" name="name" value={name} onChange={this.handleChange} required/>
          </div>

          <div className="field">
            <label>Last name:</label>
            <input type="text" name="lastName" value={lastName} onChange={this.handleChange} required/>
          </div>

          <div className="field">
            <label>Age name:</label>
            <input type="number" name="age" value={age} onChange={this.handleChange} required/>
          </div>

          <div>
            <button className="btn btn-primary">Submit</button>
            <Link to="/" className="back">Go back</Link>
          </div>

        </form>
      </div>
    )
  }
}

export default PersonForm;
