package com.nearsoft.javaschool.service;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.nearsoft.javaschool.model.Person;

@Repository
public interface PersonService extends CrudRepository<Person, Long> {
}
