package com.nearsoft.javaschool.init;


import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.nearsoft.javaschool.model.Person;
import com.nearsoft.javaschool.service.PersonService;

@Component
public class DataInitilizer {

    private PersonService personService;

    public DataInitilizer(PersonService personService) {
        this.personService = personService;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        if(personService.count() == 0) {
            personService.save(new Person("Perla", "Ruiz", 31));
            personService.save(new Person("Yander", "Caceres", 32));
            personService.save(new Person("Pedro", "Duarte", 27));
        }
    }
}
